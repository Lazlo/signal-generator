= Signal Generator =

== Features ==

 - 20 MHz base frequency
 - two separate configurable signal outputs
 - external trigger input
 - serial interface for debugging
 - ISP-6 connector for firmware upgrades
 - +5V circuit

== Hardware ==

 - 8-bit Micro-Controller driven (ATMEL AVR ATmega48)
