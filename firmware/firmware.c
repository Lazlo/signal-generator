//
// Signal Generator Firmware
//
// The firmware will use two port pins to create two separate output signals.
//
// The logic levels, frequencies and duty cycles of the output signals can be
// controlled individually from a PC using a RS-232 connection.
//
// Later on, a third port pin will be used as a external trigger input signal.
// This will allow starting the generation of the output singnals at the time
// the external trigger input becomes logic 1.
//
// This firmware is written for a ATMEL AVR ATmega48, running at 20 MHz.
//
// The hardware requirements of the firmware are:
//
//  - a 16-bit (timer1) for driving the task scheduler
//  - USART and the two respective port pins
//  - two port pins for output signals
//  - one port pin for input signal

#include <avr/io.h>
#include <avr/interrupt.h>

//--- Port pin mapping --------------------------------------------------------

// PD6 (OC0A)
#define SIG1_PIN	6
#define SIG1_PIN_DDR	DDRD
#define SIG1_PIN_PORT	PORTD

// PB3 (OC2A)
#define SIG2_PIN	3
#define SIG2_PIN_DDR	DDRB
#define SIG2_PIN_PORT	PORTB

//#define TRIG_PIN
//#define TRIG_PIN_DDR
//#define TRIG_PIN_PORT

//--- Private helper macros ---------------------------------------------------

#define SIG1_PIN_AS_OUTPUT()	(SIG1_PIN_DDR |= (1 << SIG1_PIN))
#define SIG1_PIN_OUT_ON()	(SIG1_PIN_PORT |= (1 << SIG1_PIN))
#define SIG1_PIN_OUT_OFF()	(SIG1_PIN_PORT &= ~(1 << SIG1_PIN))
#define SIG1_PIN_OUT_TOGGLE()	(SIG1_PIN_PORT ^= (1 << SIG1_PIN))

#define SIG2_PIN_AS_OUTPUT()	(SIG2_PIN_DDR |= (1 << SIG2_PIN))
#define SIG2_PIN_OUT_ON()	(SIG2_PIN_PORT |= (1 << SIG2_PIN))
#define SIG2_PIN_OUT_OFF()	(SIG2_PIN_PORT &= ~(1 << SIG2_PIN))
#define SIG2_PIN_OUT_TOGGLE()	(SIG2_PIN_PORT ^= (1 << SIG2_PIN))

//--- Private functions -------------------------------------------------------

static void pins_init(void)
{
	SIG1_PIN_AS_OUTPUT();
	SIG2_PIN_AS_OUTPUT();

	SIG1_PIN_OUT_ON();
	SIG2_PIN_OUT_ON();
}

static void timer1_init(void)
{
	uint16_t reload;

	// Calculate reload value (for 1 milisecond).
	reload = F_CPU / 1000;

	// Set timer reload value.
	OCR1AH = reload >> 8;
	OCR1AL = reload;

	// Set timer mode to CTC.
	TCCR1B |= (1 << WGM12);

	// Enable timer interrupt.
	TIMSK1 |= (1 << OCIE1A);

	// Start timer by selecting a clock source (F_OSC/1).
	TCCR1B |= (1 << CS10);
}

//--- Public functions --------------------------------------------------------

int main(void)
{
	// Setup timer1 for periodic interrupts at 1 milisecond.
	timer1_init();

	// Setup port pins.
	pins_init();

	// Enable interrupts globally.
	sei();

	while(1)
		;

	return 0;
}

ISR(TIMER1_COMPA_vect)
{
	SIG1_PIN_OUT_TOGGLE();
	SIG2_PIN_OUT_TOGGLE();
}
