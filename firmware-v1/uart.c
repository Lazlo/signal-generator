/**
 * @file
 */

///////////////////////////////////////////////////////////////////////////////
//
// Includes
//
///////////////////////////////////////////////////////////////////////////////

#include <avr/io.h>

///////////////////////////////////////////////////////////////////////////////
//
// Private Macros
//
///////////////////////////////////////////////////////////////////////////////

#define DataRegister_IsEmpty()	(*s_ptr_reg_ucsra & (1 << UDRE0))

#define DataRegister_Write(c)	(*s_ptr_reg_udr = c)

///////////////////////////////////////////////////////////////////////////////
//
// Private Variables
//
///////////////////////////////////////////////////////////////////////////////

static volatile uint8_t *s_ptr_reg_udr;
static volatile uint8_t *s_ptr_reg_ubrrh;
static volatile uint8_t *s_ptr_reg_ubrrl;
static volatile uint8_t *s_ptr_reg_ucsra;
static volatile uint8_t *s_ptr_reg_ucsrb;

///////////////////////////////////////////////////////////////////////////////
//
// Private Functions
//
///////////////////////////////////////////////////////////////////////////////

/** Calculate the prescaler value that is feed into the Clock Generator 'Baud Rate' registers.
 *
 * \param f_cpu Main oscillator frequency in Hz.
 * \param baudRate Requested baud rate.
 * \param mode UART mode (either asynchronous or synchronous).
 *
 * \return 16-bit Value to be written into the Clock Generator 'Baud Rate' registers.
 */
static uint16_t ubrr_calc(const uint32_t f_cpu, const uint32_t baudRate, const uint8_t mode)
{
	return (f_cpu / (16 * baudRate)) - 1;
}

static void ubrr_write(uint16_t ubrr)
{
	*s_ptr_reg_ubrrh = ubrr >> 8;
	*s_ptr_reg_ubrrl = ubrr;
}

static void transciever_enable(void)
{
	*s_ptr_reg_ucsrb |= (1 << RXEN0)|(1 << TXEN0);
}

///////////////////////////////////////////////////////////////////////////////
//
// Private Functions
//
///////////////////////////////////////////////////////////////////////////////

/** Initialize UART device.
 *
 * \param baudRate Requested baud rate.
 */
void UART_Init(const uint16_t baudRate)
{
	/** UART mode (asynchronous or synchonous) */
	uint8_t mode;

	/** Value use for the Clock Generator prescaler. */
	uint16_t ubrr;


	/* Initialize pointers to UART registers. */
	s_ptr_reg_udr = &UDR0;
	s_ptr_reg_ubrrh = &UBRR0H;
	s_ptr_reg_ubrrl = &UBRR0L;
	s_ptr_reg_ucsra = &UCSR0A;
	s_ptr_reg_ucsrb = &UCSR0B;


	///////////////////////////////////////////////////////////////////////
	//
	// Configure Clock Generator
	//
	///////////////////////////////////////////////////////////////////////

	// Calculate baud rate register value.
	ubrr = ubrr_calc(F_CPU, baudRate, mode);

	// Write value to baud rate register.
	ubrr_write(ubrr);

	///////////////////////////////////////////////////////////////////////
	//
	// Enable Transmitter/Receiver
	//
	///////////////////////////////////////////////////////////////////////

	/* Enable receiver and transmitter. */
	transciever_enable();

	///////////////////////////////////////////////////////////////////////
	//
	// Configure Frame Format
	//
	///////////////////////////////////////////////////////////////////////

	/* Register hardware default after reset is:
	 * - mode: asynchronous operation
	 * - parity: disabled
	 * - stop bits: 1-bit
	 * - character size: 8-bit
	 */
}

void UART_Putc(const char c)
{
	// Wait while 'data register empty' status bit _NOT_ set.
	while( ! DataRegister_IsEmpty() )
		;

	// Write byte to transmit data register.
	DataRegister_Write(c);
}

void UART_Puts(char *s)
{
	// While char is not a string terminator
	while(*s != '\0')
		// Call putchar and post-increment pointer address.
		UART_Putc(*s++);
}
