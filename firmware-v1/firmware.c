/**
 * @file
 */

/*
 * Signal Generator Firmware
 *
 * Will initialize the hardware timers for the two output signals and the
 * external trigger input.
 */

#include <avr/interrupt.h>	/* for sei() */
#include <avr/sleep.h>		/* for sleep_mode() */

#include "uart.h"
#include "timer.h"
#include "uptime.h"

int main(void)
{
	/* Setup serial interface for debugging. */
	UART_Init(38400);
	UART_Puts("UART ready!\r\n");

	/* Setup the 16-bit timer1 for waking up the system every second. */
	Timer1_Init(1000);

	/* Setup the two 8-bit timers timer0 and timer2. */
	Timer0_Init(5); /* prescaler 1024 */
	Timer2_Init(7); /* prescaler 1024 */

	/* Setup uptime software module. */
	UpTime_Init(0, 0, 0);

	/* Enable interrupts globally. */
	sei();

	/* Loop forever and sleep. */
	while(1)
	{
		/* Put system to sleep. */
		sleep_mode();
	}

	return 0;
}

/* Interrupt handler for Timer1 Compare Match. */
ISR(TIMER1_COMPA_vect)
{
	/* Storage for the uptime string. */
	char str[9];

	/* Call uptime module tick function once per second. */
	UpTime_Tick();

	/* Get current uptime as string. */
	UpTime_ToStr(str);

	/* Write uptime string to serial interface. */
	UART_Puts(str);
	UART_Puts("\r\n");
}
