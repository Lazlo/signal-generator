/**
 * @file
 */

#include <inttypes.h>

static uint8_t s_hour;
static uint8_t s_minute;
static uint8_t s_second;

void UpTime_Init(const uint8_t h, const uint8_t m, const uint8_t s)
{
	s_hour = h;
	s_minute = m;
	s_second = s;
}

void UpTime_Tick(void)
{
	if(s_second++ == 59)
	{
		s_second = 0;

		if(s_minute++ == 59)
		{
			s_minute = 0;

			if(s_hour++ == 23)
			{
				s_hour = 0;
			}
		}
	}
}

void UpTime_ToStr(char *str)
{
	const char charmap[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	// Hours
	str[0] = charmap[s_hour / 10];
	str[1] = charmap[s_hour % 10];

	// Separator
	str[2] = ':';

	// Minutes
	str[3] = charmap[s_minute / 10];
	str[4] = charmap[s_minute % 10];

	// Separator
	str[5] = ':';

	// Seconds
	str[6] = charmap[s_second / 10];
	str[7] = charmap[s_second % 10];

	// String terminator
	str[8] = '\0';
}
