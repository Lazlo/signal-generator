/**
 * @file
 */

#ifndef D_TIMER_H
#define D_TIMER_H

void Timer0_Init(const uint8_t cs);

void Timer1_Init(const uint16_t ms);

void Timer2_Init(const uint8_t cs);

#endif /* D_TIMER_H */
