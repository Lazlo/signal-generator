/**
 * @file
 */

#ifndef D_UPTIME_H
#define D_UPTIME_H

void UpTime_Init(const uint8_t h, const uint8_t m, const uint8_t s);

void UpTime_Tick(void);

void UpTime_ToStr(char *str);

#endif /* D_UPTIME_H */
