/**
 * @file
 */

#include <inttypes.h>
#include <avr/io.h>

///////////////////////////////////////////////////////////////////////////////
//
// Private variables
//
///////////////////////////////////////////////////////////////////////////////

static volatile uint8_t *s_ptr_t0_reg_tccra;
static volatile uint8_t *s_ptr_t0_reg_tccrb;

static volatile uint8_t *s_ptr_t1_reg_tccrb;
static volatile uint8_t *s_ptr_t1_reg_ocrah;
static volatile uint8_t *s_ptr_t1_reg_ocral;
static volatile uint8_t *s_ptr_t1_reg_timsk;

static volatile uint8_t *s_ptr_t2_reg_tccra;
static volatile uint8_t *s_ptr_t2_reg_tccrb;

///////////////////////////////////////////////////////////////////////////////
//
// Private functions
//
///////////////////////////////////////////////////////////////////////////////

/*
 * Mode Selection
 */

static void t0_set_mode_1(void)
{
	*s_ptr_t0_reg_tccra |= (1 << COM0A0); /* Toggle OC0A on Compare Match. */
}

static void t1_set_mode_ctc(void)
{
	*s_ptr_t1_reg_tccrb |= (1 << WGM12);
}

static void t2_set_mode_1(void)
{
	*s_ptr_t2_reg_tccra |= (1 << COM2A0); /* Toggle OC2A on Compare Match. */
}

/*
 * Clock Source Selection
 */

static void t0_set_cs(const uint8_t cs)
{
	volatile uint8_t *reg;

	reg = s_ptr_t0_reg_tccrb;

	switch(cs)
	{
	case 0: *reg &= ((1 << CS02)|(1 << CS01)|(1 << CS00));	break; /* none - stopped. */
	case 1:	*reg |= (1 << CS00);				break; /* 1 */
	case 2:	*reg |= (1 << CS01);				break; /* 8 */
	case 3:	*reg |= (1 << CS01)|(1 << CS00);		break; /* 64 */
	case 4:	*reg |= (1 << CS02);				break; /* 256 */
	case 5:	*reg |= (1 << CS02)|(1 << CS00);		break; /* 1024 */
	case 6:	*reg |= (1 << CS02)|(1 << CS01);		break; /* from T0 pin on falling edge */
	case 7: *reg |= (1 << CS02)|(1 << CS01)|(1 << CS00);	break; /* from T0 pin on rising edge */
	}
}

static void t1_set_cs(const uint8_t cs)
{
	volatile uint8_t *reg;

	reg = s_ptr_t1_reg_tccrb;

	switch(cs)
	{
	case 5:	*reg |= (1 << CS12)|(1 << CS10);		break; /* 1024 */
	}
}

static void t2_set_cs(const uint8_t cs)
{
	volatile uint8_t *reg;

	reg = s_ptr_t2_reg_tccrb;

	switch(cs)
	{
	case 0: *reg &= ~((1 << CS22)|(1 << CS21)|(1 << CS20));	break; /* none - stopped. */
	case 1: *reg |= (1 << CS20);				break; /* 1 */
	case 2: *reg |= (1 << CS21);				break; /* 8 */
	case 3: *reg |= (1 << CS21)|(1 << CS20);		break; /* 32 */
	case 4: *reg |= (1 << CS22);				break; /* 64 */
	case 5: *reg |= (1 << CS22)|(1 << CS20);		break; /* 128 */
	case 6: *reg |= (1 << CS22)|(1 << CS21);		break; /* 256 */
	case 7: *reg |= (1 << CS22)|(1 << CS21)|(1 << CS20);	break; /* 1024 */
	}
}

/*
 * Compare Match Value
 */

static void t1_set_reload_value(uint16_t reload)
{
	*s_ptr_t1_reg_ocrah = reload >> 8;
	*s_ptr_t1_reg_ocral = reload;
}

/*
 * Interrupt Enable Bits
 */

static void t1_set_match_interrupt_enable(void)
{
	*s_ptr_t1_reg_timsk |= (1 << OCIE1A);
}

///////////////////////////////////////////////////////////////////////////////
//
// Public functions
//
///////////////////////////////////////////////////////////////////////////////

void Timer0_Init(const uint8_t cs)
{
	/* Initialize pointers to registers. */
	s_ptr_t0_reg_tccra = &TCCR0A;
	s_ptr_t0_reg_tccrb = &TCCR0B;

	/* Make OC0A pin an output. */
	DDRD |= (1 << PD6);

	/* Set Waveform Generation Mode. */

	/* Set Output Compare Mode. */
	t0_set_mode_1();

	/* Enable Compare Match interrupt. */
//	TIMSK0 |= (1 << OCIE0A);

	/* Start timer by selecting a clock source. */
	t0_set_cs(cs);
}

void Timer1_Init(const uint16_t ms)
{
	uint16_t reload;

	/* Initialize pointers to registers. */
	s_ptr_t1_reg_tccrb = &TCCR1B;
	s_ptr_t1_reg_ocrah = &OCR1AH;
	s_ptr_t1_reg_ocral = &OCR1AL;
	s_ptr_t1_reg_timsk = &TIMSK1;

	/* Calculate timer reload value. */
	reload = F_CPU / 1024 / 1000 * ms;

	/* Write reload value. */
	t1_set_reload_value(reload);

	/* Set timer mode to CTC. */
	t1_set_mode_ctc();

	/* Enable timer compare match interrupt. */
	t1_set_match_interrupt_enable();

	/* Start timer by selecting a clock source. */
	t1_set_cs(5); /* 1024 */
}

void Timer2_Init(const uint8_t cs)
{
	/* Initialize pointers to registers. */
	s_ptr_t2_reg_tccra = &TCCR2A;
	s_ptr_t2_reg_tccrb = &TCCR2B;

	/* Make OC2A pin an output. */
	DDRB |= (1 << PB3);

	/* Set Output Compare Mode. */
	t2_set_mode_1();

	/* Start timer by selecting a clock source. */
	t2_set_cs(cs);
}
